@extends('layout.home')
@section('content')
<div class="card">
<div class="card-header">
    <p class="text-dark" style="text-align: left; font-size:16px; margin-top:15px">Mungkin grup dapat membantu kamu mengetahui sesuatu.<br>
    </p>
    <a href="/forum/create" type="submit" class="btn btn-success text-white btn-md text-dark" style="border-radius:10px; margin-top:10px; margin-bottom:10px;">Mulai Bertanya</a>
</div>
    @foreach ($pertanyaan as $tanya)
    <div class="inner-main-body pl-4 pr-4 mt-3">
                <div class="card mb-2">
                    <div class="card-body">
                        <div class="media forum-item">
                            <a href="" class="text-end rounded-circle pr-3 pl-3 pt-2 pb-2 mr-3 btn-light" style="font-weight:bold;font-size:16px;" data-toggle="collapse" data-target=".forum-content">
                                {{ strtoupper(substr($tanya->user->profile->nama_lengkap, 0, 1)) }}
                            </a>
                            <div class="media-body">
                                <h6><a href="/forum/show/{{$tanya->id}}"  class="text-bold">{{$tanya->judul}}</a></h6>
                                {{-- <p class="text-secondary">
                                    {!!$tanya->isi!!}
                                </p> --}}
                                <p class="text-muted"><a href="javascript:void(0)">Diposting </a><span class="text-secondary font-weight-bold">{{$tanya->created_at->diffForHumans()}}</span></p>
                            </div>
                            <div class="text-muted small text-center align-self-center">
                                <span>Tags :</span>
                                @foreach ($tanya->tags as $tag)
                                <button class="btn btn-primary btn-sm">{{$tag->tag_name ? $tag->tag_name:'No tags' }}</button>
                                @endforeach
                                <span><i class="far fa-comment ml-2"></i> {{$tanya->jawaban->count()}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
    @endforeach

</div>
@endsection
