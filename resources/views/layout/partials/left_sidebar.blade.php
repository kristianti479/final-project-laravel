    <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #ffffff; background: ">
        <!-- Brand Logo -->
        <div class="user-panel">
            <a href="#" class="brand-link" style="display: inline-block;margin: 0px 0;">
            <span class="brand-text text-green" style="font-weight:bold;"> WaguApps </span>
            <span class="text-dark" style="font-weight:bold;font-size:20px;"> | </span>
            <span class="text-dark" style="font-size:12px;"> Waktunya Berguru</span>
            </a>
        </div>

        <!-- Sidebar -->
        <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-1 pb-1 mb-1 d-flex">
            <div class="info nav nav-pills nav-sidebar flex-column"  data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview">
                <a href="#" class="nav-link media pl-1">
                    <div>
                        <i class="far fa-user-circle mr-1" style="font-size:36px;color:#000000"></i>
                    </div>
                    <div class="media-body pl-1">
                        <h3 class="text-dark" style="font-size:18px;">
                        {{ Auth::user()->name }}
                        
                        </h3>
                        <p class="text-sm text-green">Online</p>
                    </div>
                </a>
                </li>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="nav flex-column">
            @if (Auth::user()->role==='admin')
                <a href="/profile" class="btn btn-info text-light" onclick="" style="margin-top:5px;">
                    Admin Panel
                </a>
            @endif
                <a href="/logout" class="btn btn-light text-dark" onclick="" style="margin-top:5px;">
                    Log out
                </a>
        </nav>
        <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
